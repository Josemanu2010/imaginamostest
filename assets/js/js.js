
$(document).ready(function(){

	var categories = [

		{
			"id": 1,
			"name": "All",
			"icon": "https://image.flaticon.com/icons/svg/701/701965.svg"
		},
		{
			"id": 2,
			"name": "Pizza",
			"icon": "https://image.flaticon.com/icons/svg/599/599995.svg"
		},
		{
			"id": 3,
			"name": "Asian",
			"icon": "https://image.flaticon.com/icons/svg/1895/1895684.svg"
		},
		{
			"id": 4,
			"name": "Burgers",
			"icon": "https://image.flaticon.com/icons/svg/883/883806.svg"
		},
		{
			"id": 5,
			"name": "Barbecue",
			"icon": "https://image.flaticon.com/icons/svg/933/933310.svg"
		},
		{
			"id": 6,
			"name": "Dessers",
			"icon": "https://image.flaticon.com/icons/svg/174/174394.svg"
		},
		{
			"id": 7,
			"name": "Thai",
			"icon": "https://image.flaticon.com/icons/svg/135/135367.svg"
		},
		{
			"id": 8,
			"name": "Sushi",
			"icon": "https://image.flaticon.com/icons/svg/1900/1900683.svg"
		}
	]


	$('.p1').css('background', 'url("'+categories[0].icon+'") no-repeat center');
	$('.p2').css('background', 'url("'+categories[1].icon+'") no-repeat center');
	$('.p3').css('background', 'url("'+categories[2].icon+'") no-repeat center');
	$('.p4').css('background', 'url("'+categories[3].icon+'") no-repeat center');
	$('.p5').css('background', 'url("'+categories[4].icon+'") no-repeat center');
	$('.p6').css('background', 'url("'+categories[5].icon+'") no-repeat center');


	$('.p1').on('mouseover', function(){
		$('.p1').css('transition', '.9s');
		$('.p1').css('background', '#e65100');
	});
	$('.p1').on('mouseleave', function(){
		$('.p1').css('background', 'url("'+categories[0].icon+'") no-repeat center');
		$('.p1').css('transition', '.5s');
	});

	$('.p2').on('mouseover', function(){
		$('.p2').css('transition', '.9s');
		$('.p2').css('background', '#e65100');
	});
	$('.p2').on('mouseleave', function(){
		$('.p2').css('background', 'url("'+categories[1].icon+'") no-repeat center');
		$('.p2').css('transition', '.5s');
	});

	$('.p3').on('mouseover', function(){
		$('.p3').css('transition', '.9s');
		$('.p3').css('background', '#e65100');
	});
	$('.p3').on('mouseleave', function(){
		$('.p3').css('background', 'url("'+categories[2].icon+'") no-repeat center');
		$('.p3').css('transition', '.5s');
	});

	$('.p4').on('mouseover', function(){
		$('.p4').css('transition', '.9s');
		$('.p4').css('background', '#e65100');
	});
	$('.p4').on('mouseleave', function(){
		$('.p4').css('background', 'url("'+categories[3].icon+'") no-repeat center');
		$('.p4').css('transition', '.5s');
	});

	$('.p5').on('mouseover', function(){
		$('.p5').css('transition', '.9s');
		$('.p5').css('background', '#e65100');
	});
	$('.p5').on('mouseleave', function(){
		$('.p5').css('background', 'url("'+categories[4].icon+'") no-repeat center');
		$('.p5').css('transition', '.5s');
	});

	$('.p6').on('mouseover', function(){
		$('.p6').css('transition', '.9s');
		$('.p6').css('background', '#e65100');
	});
	$('.p6').on('mouseleave', function(){
		$('.p6').css('background', 'url("'+categories[5].icon+'") no-repeat center');
		$('.p6').css('transition', '.5s');
	});
	
	

	$('#cardImg1').on('mouseenter', function(){
		$('#saleOff1').html('<h6 class="text-success"><div class="badge badge-danger">30%</div> OFF Get Product</h6>');
	});
	$('#cardImg1').on('mouseleave', function(){
		$('#saleOff1').html('');
	});
	$('#cardImg2').on('mouseenter', function(){
		$('#saleOff2').html('<h6 class="text-success"><div class="badge badge-danger">10%</div> OFF Get Product</h6>');
	});
	$('#cardImg2').on('mouseleave', function(){
		$('#saleOff2').html('');
	});
	$('#cardImg3').on('mouseenter', function(){
		$('#saleOff3').html('<h6 class="text-success"><div class="badge badge-danger">22%</div> OFF Get Product</h6>');
	});
	$('#cardImg3').on('mouseleave', function(){
		$('#saleOff3').html('');
	});


})